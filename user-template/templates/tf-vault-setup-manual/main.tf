terraform {
  backend "http" {
  }
  required_version = "~> 1.2"
}

provider "vault" {
  skip_child_token = true
}

module "prod-review-env" {
  source = "git::https://gitlab.com/geekstuff.it/libs/terraform/vault/gitlab-pipeline/flavored/prod-review?ref=v0.0.3"

  project_name = var.project_name

  // will add 1 "policy rule" to read each secrets, for each environment, under a single vault "policy".
  read_secrets = [
    {
      name        = "pipeline"
      description = "single secret for gitlab-ci pipeline"
    }
  ]

  // gitlab project ID to give this pipeline access to
  gitlab_vault_jwt_project_id = var.gitlab_vault_jwt_project_id

  // The TF Lib we are using will create a transit key per environment by default
  // Uncomment the following to NOT create any transit keys.
  # transit_keys = []
}
