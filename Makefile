#!make

# NOTE: This Makefile is made with local dev usage in mind

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

ALPINE_SOURCE ?= alpine
ALPINE_TAG ?= latest

DEBIAN_SOURCE ?= debian
DEBIAN_TAG ?= bullseye-slim

DEVCONTAINER_VERSION ?= v0.0.0
DEVCONTAINER_PREFIX ?= devcontainer/${DEVCONTAINER_VERSION}/terraform/gitlab-managed-state/

all: info

.PHONY: info
info:
	@echo "Welcome to Lib tf.gitlab-managed-state"
	@echo ""
	@echo "Useful commands:"
	@echo "  make docker.alpine"
	@echo "  make docker.debian"

.PHONY: .docker.devcontainer
.docker.devcontainer:
	docker build \
		-t ${DEVCONTAINER_PREFIX}${FROM_SOURCE}:${FROM_TAG} \
		-f devcontainer/Dockerfile \
		--build-arg BUILD_TAG=${DEVCONTAINER_VERSION} \
		--build-arg FROM_SOURCE=${FROM_SOURCE} \
		--build-arg FROM_TAG=${FROM_TAG} \
		.

.PHONY: docker.alpine
docker.alpine:
	FROM_SOURCE=${ALPINE_SOURCE} \
	FROM_TAG=${ALPINE_TAG} \
		$(MAKE) .docker.devcontainer

.PHONY: docker.debian
docker.debian:
	FROM_SOURCE=${DEBIAN_SOURCE} \
	FROM_TAG=${DEBIAN_TAG} \
		$(MAKE) .docker.devcontainer
